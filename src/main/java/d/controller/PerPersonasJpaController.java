/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d.controller;

import d.controller.exceptions.NonexistentEntityException;
import d.controller.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.entity.PerPersonas;

/**
 *
 * @author xtian
 */
public class PerPersonasJpaController implements Serializable {

    public PerPersonasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PerPersonas perPersonas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(perPersonas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPerPersonas(perPersonas.getRut()) != null) {
                throw new PreexistingEntityException("PerPersonas " + perPersonas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PerPersonas perPersonas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            perPersonas = em.merge(perPersonas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = perPersonas.getRut();
                if (findPerPersonas(id) == null) {
                    throw new NonexistentEntityException("The perPersonas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PerPersonas perPersonas;
            try {
                perPersonas = em.getReference(PerPersonas.class, id);
                perPersonas.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The perPersonas with id " + id + " no longer exists.", enfe);
            }
            em.remove(perPersonas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PerPersonas> findPerPersonasEntities() {
        return findPerPersonasEntities(true, -1, -1);
    }

    public List<PerPersonas> findPerPersonasEntities(int maxResults, int firstResult) {
        return findPerPersonasEntities(false, maxResults, firstResult);
    }

    private List<PerPersonas> findPerPersonasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PerPersonas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PerPersonas findPerPersonas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PerPersonas.class, id);
        } finally {
            em.close();
        }
    }

    public int getPerPersonasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PerPersonas> rt = cq.from(PerPersonas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
